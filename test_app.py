from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def greet():
    greetings = 'Flask server working'
    return render_template('test_index.html', greetings=greetings)

@app.route('/test-routing')
def routing():
    greetings = 'Successfully routed request within the flask app'
    return render_template('test_index.html', greetings=greetings)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)
